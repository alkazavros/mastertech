﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using MasterTech.Controllers;
using MasterTech.Models;
using Xunit;
using MasterTech.Models.ViewModels;


namespace MasterTech.Tests
{

    public class ProductControllerTests
    {
        [Fact]
        public void Can_Paginate()
        {
            // Arrange
            Mock<IServiceRepository> mock = new Mock<IServiceRepository>();
            mock.Setup(m => m.Services).Returns((new Service[] {
                new Service {ServiceID = 1, Name = "P1"},
                new Service {ServiceID = 2, Name = "P2"},
                new Service {ServiceID = 3, Name = "P3"},
                new Service {ServiceID = 4, Name = "P4"},
                new Service {ServiceID = 5, Name = "P5"}
            }).AsQueryable<Service>());

            ServiceController controller = new ServiceController(mock.Object);
            controller.PageSize = 3;

            // Act
            ServicesListViewModel result =
                controller.List(null, 2).ViewData.Model as ServicesListViewModel;

            // Assert
            Service[] serviceArray = result.Services.ToArray();
            Assert.True(serviceArray.Length == 2);
            Assert.Equal("P4", serviceArray[0].Name);
            Assert.Equal("P5", serviceArray[1].Name);
        }
    }
}
