﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Moq;
using MasterTech.Components;
using MasterTech.Models;
using Xunit;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;

namespace MasterTech.Tests
{
    public class NavigationMenuViewComponentTests
    {
        [Fact]
        public void Can_Select_Categories()
        {
            // Arrange
            Mock<IServiceRepository> mock = new Mock<IServiceRepository>();
            mock.Setup(m => m.Services).Returns((new Service[] {
                new Service {ServiceID = 1, Name = "P1", Category = "Apples"},
                new Service {ServiceID = 2, Name = "P2", Category = "Apples"},
                new Service {ServiceID = 3, Name = "P3", Category = "Plums"},
                new Service {ServiceID = 4, Name = "P4", Category = "Oranges"},
            }).AsQueryable<Service>());

            NavigationMenuViewComponent target =
                new NavigationMenuViewComponent(mock.Object);

            // Act = get the set of categories
            string[] results = ((IEnumerable<string>)(target.Invoke()
                as ViewViewComponentResult).ViewData.Model).ToArray();

            // Assert
            Assert.True(Enumerable.SequenceEqual(new string[] { "Apples",
                "Oranges", "Plums" }, results));
        }
    }
}
