﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using MasterTech.Models;

namespace MasterTech.Components
{

    public class NavigationMenuViewComponent : ViewComponent
    {
        private IServiceRepository repository;

        public NavigationMenuViewComponent(IServiceRepository repo)
        {
            repository = repo;
        }

        public IViewComponentResult Invoke()
        {
            ViewBag.SelectedCategory = RouteData?.Values["category"];
            return View(repository.Services
                .Select(x => x.Category)
                .Distinct()
                .OrderBy(x => x));
        }
    }
}
