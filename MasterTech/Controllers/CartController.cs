﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterTech.Infrastructure;
using MasterTech.Models;
using MasterTech.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace MasterTech.Controllers
{
    public class CartController : Controller
    {
        private IServiceRepository repository;

        public CartController(IServiceRepository repo)
        {
            repository = repo;
        }

        public ViewResult Index(string returnUrl)
        {
            return View(new CartIndexViewModel
            {
                Cart = GetCart(),
                ReturnUrl = returnUrl
            });
        }

        public RedirectToActionResult AddToCart(int serviceid, string returnUrl)
        {
            Service service = repository.Services
                .FirstOrDefault(s => s.ServiceID == serviceid);

            if (service != null)
            {
                Cart cart = GetCart();
                cart.AddItem(service, 1);
                SaveCart(cart);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        public RedirectToActionResult RemoveFromCart(int serviceid,
                string returnUrl)
        {
            Service service = repository.Services
                .FirstOrDefault(s => s.ServiceID == serviceid);

            if (service != null)
            {
                Cart cart = GetCart();
                cart.RemoveLine(service);
                SaveCart(cart);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        private Cart GetCart()
        {
            Cart cart = HttpContext.Session.GetJson<Cart>("Cart") ?? new Cart();
            return cart;
        }

        private void SaveCart(Cart cart)
        {
            HttpContext.Session.SetJson("Cart", cart);
        }
    }
}
