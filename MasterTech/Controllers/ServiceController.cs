﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterTech.Models;
using Microsoft.AspNetCore.Mvc;
using MasterTech.Models.ViewModels;

namespace MasterTech.Controllers
{
    public class ServiceController : Controller
    {
        private IServiceRepository repository;
        public int PageSize = 4;
        public ServiceController(IServiceRepository repo)
        {
            repository = repo;   
        }
        public ViewResult List(string category, int servicePage = 1) =>
            View(new ServicesListViewModel
            {
                Services = repository.Services
                    .Where(s => category == null || s.Category == category)
                    .OrderBy(s => s.ServiceID)
                    .Skip((servicePage - 1) * PageSize)
                    .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = servicePage,
                    ItemsPerPage = PageSize,
                    TotalItems = category == null ?
                    repository.Services.Count() :
                    repository.Services.Where(e =>
                    e.Category == category).Count()
                },
                CurrentCategory = category
            });
    }
}
