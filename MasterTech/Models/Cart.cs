﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MasterTech.Models
{
    public class Cart
    {
        private List<CartLine> lineCollection = new List<CartLine>();

        public virtual void AddItem(Service service, int quantity)
        {
            CartLine line = lineCollection
                .Where(s => s.Service.ServiceID == service.ServiceID)
                .FirstOrDefault();

            if (line == null)
            {
                lineCollection.Add(new CartLine
                {
                    Service = service
                });
            }
            else
            {
            }
        }

        public virtual void RemoveLine(Service service) =>
            lineCollection.RemoveAll(l => l.Service.ServiceID == service.ServiceID);

        public virtual decimal ComputeTotalValue() =>
            lineCollection.Sum(e => e.Service.Price);

        public virtual void Clear() => lineCollection.Clear();

        public virtual IEnumerable<CartLine> Lines => lineCollection;
    }

    public class CartLine
    {
        public int CartLineID { get; set; }
        public Service Service { get; set; }
    }
}
