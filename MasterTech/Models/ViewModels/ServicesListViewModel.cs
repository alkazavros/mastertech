﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterTech.Models;

namespace MasterTech.Models.ViewModels
{
    public class ServicesListViewModel
    {
        public IEnumerable<Service> Services { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public string CurrentCategory { get; set; }
    }
}
