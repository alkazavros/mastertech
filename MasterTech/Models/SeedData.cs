﻿using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;


namespace MasterTech.Models
{
    public static class SeedData
    {
        public static void EnsurePopulated (IApplicationBuilder app)
        {
            ApplicationDbContext context = app.ApplicationServices
                .GetRequiredService<ApplicationDbContext>();
            context.Database.Migrate();
            if (!context.Services.Any())
            {
                context.Services.AddRange(
                        new Service
                        {
                            Name = "Замена комплекта сцпеления",
                            Description = "Корзина сцепления, выжимной подшипник, диск сцепления",
                            Category = "КПП",
                            Price = 1200
                        },
                        new Service
                        {
                            Name = "Замена масла",
                            Description = "Определения уровня масла, проверки его состояния и цвета, замена",
                            Category = "КПП",
                            Price = 800
                        },
                        new Service
                        {
                            Name = "Диагностика БУ КПП",
                            Description = "Подключение АКПП к электрическому блоку управления для считывания кодов ошибок",
                            Category = "КПП",
                            Price = 900
                        },
                        new Service
                        {
                            Name = "Замер компрессии",
                            Description = "Диагностика для вычисления неисправных цилиндров",
                            Category = "Двигатель",
                            Price = 500
                        },
                        new Service
                        {
                            Name = "Эндоскопия",
                            Description = "Осмотр состояния стенок цилиндров специализированным видеооборудованием",
                            Category = "Двигатель",
                            Price = 1150
                        },
                        new Service
                        {
                            Name = "Регулировка клапанов",
                            Description = " процесс, обеспечивающий идеальное прилегание впускного и выпускного клапана в цилиндрах двигателя на их посадочных местах (седлах)",
                            Category = "Двигатель",
                            Price = 450
                        },
                        new Service
                        {
                            Name = "Замена прокладки ГБЦ",
                            Description = "Прокладка головки блока цилиндров обеспечивает герметизацию камеры сгорания, герметизирует каналы системы охлаждения, по которым циркулирует охлаждающая жидкость",
                            Category = "Двигатель",
                            Price = 670
                        },
                        new Service
                        {
                            Name = "Балансировка дисков",
                            Description = "Процедура уменьшения до приемлемого уровня дисбаланса колеса, его крепления, элементов подвески, диска и ступицы",
                            Category = "Шиномонтаж",
                            Price = 280
                        },
                        new Service
                        {
                            Name = "Ремонт прокола",
                            Description = "Установка герметизирующей прокладки на бескамерную покрышку",
                            Category = "Шиномонтаж",
                            Price = 30
                        },
                        new Service
                        {
                            Name = "Разбортировка колеса",
                            Description = "Демонтаж покрышки с диска",
                            Category = "Шиномонтаж",
                            Price = 130
                        },
                        new Service
                        {
                            Name = "Бортировка колеса",
                            Description = "Установка покрышки на диск",
                            Category = "Шиномонтаж",
                            Price = 110
                        },
                        new Service
                        {
                            Name = "Диагностика подвески",
                            Description = "Проверка всех рычагов, сайлентблоков, направляющих, стабилизаторов, тяг, полуосей подвески",
                            Category = "Подвеска",
                            Price = 250
                        }
                    );
                context.SaveChanges();
            }
        }
    }
}
