﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MasterTech.Models
{
    public class EFServiceRepository : IServiceRepository
    {
        private ApplicationDbContext context;
        public EFServiceRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }
        public IQueryable<Service> Services => context.Services;
    }
}
