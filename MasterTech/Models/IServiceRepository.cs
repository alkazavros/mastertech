﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MasterTech.Models
{
    public interface IServiceRepository
    {
        IQueryable<Service> Services { get; }
    }
}
